THE BOOK OF JOSHUA

CHAPTER 1

NOW aftert the death of Moses the servant of the LORD it came to pass,
that the LORD spake unto Joshua the son of Nun, Moses' minister, saying,
 2 Moses my servant is dead; now therefore arise, go over this Jordan,
thou, and all this people, unto the land which I do give to them, _even_
to the children of Israel.
 3 Every place that the sole of your foot shall tread upon, that have I
given unto you, as I said unto Moses.
 4 From the wilderness and this Lebanon even unto the great river, the
river Euphrates, all the land of the Hittites, and unto the great sea
toward the going down of the sun, shall be your coast.
 5 There shall not any man be able to stand before thee all the days of
thy life: as I was with Moses, _so_ I will be with thee: I will not fail
thee, nor forsake thee.
 6 Be strong and of a good courage: for unto this people shalt thou
divide for an inheritance the land, which I sware unto their fathers to
give them.
 7 Only be thou strong and very courageous, that thou mayest observe to
do according to all the law, which Moses my servant commanded thee: turn
not from it _to_ the right hand or _to_ the left, that thou mayest
prosper whithersoever thou goest.
 8 This book of the law shall not depart out of thy mouth; but thou
shalt meditate therein day and night, that thou mayest observe to do
according to all that is written therein: for then thou shalt make thy
way prosperous, and then thou shalt have good success.
 9 Have not I commanded thee? Be strong and of a good courage; be not
afraid, neither be thou dismayed: for the whithersoever thou goest.
 10 ¶ Then Joshua commanded the officers of the people, saying,
 11 Pass through the host, and command the people, saying, Prepare you
victuals; for within three days ye shall pass over this Jordan, to go in
to possess the land, which the LORD your God giveth you to possess it.
 12 ¶ And to the Reubenites, and to theGadites, and to half the tribe of
Manasseh, spake Joshua, saying,
 13 Remember the word which Moses the servant of the LORD commanded you,
saying, The LORD your God hath given you rest, and hath given you this
land.
 14 Your wives, your little ones, and your cattle, shall remain in the
land which Moses gave you on this side Jordan; but ye shall pass before
your brethren armed, all the mighty men of vaour, and help them;
 15 Until the LORD have given your brethren rest, as _he hath given_
you, and they also have possessed the land which the LORD your God
giveth them: then ye shall return unto the land of your possession, and
enjoy it, which Moses the LORD'S servant gave you on this side Jordan
toward the sunrising.
 16 ¶ And they answered Joshua, saying, All that thou commandest us we
will do, whithersoever thou sendest us, we will go.
 17 According as we hearkened unto Moses in all things, so will we
hearken unto thee: only the LORD thy God be with thee, as he was with
Moses.
 18 Whosoever _he be_ that doth rebel against thy commandment, and will
not hearken unto thy words in all that thou commandest him, he shall be
put to death: only be strong and of a good courage.

CHAPTER 2

AND Joshua the son of Nun sent out of Shittim two men to spy secretly,
saying, Go view the land, even Jericho.
 2 And it was told the king of Jericho, saying, Behold, there came men
hither to night of the children of Israel to search out the country.
 3 And the king of Jericho sent unto Rahab, saying, Bring forth the men
that are come to thee, which are entered into thine house: for they be
come to search out all the country.
 4 And the woman took the two men, and hid them, and said thus, There
came men unto me, but I wist not whence they _were._
 5 And it came to pass _about the time_ of shutting of the gate, when it
was dark, that the men went out: whither the men went I wot not: pursue
after them quickly; for ye shall overtake them.
 6 But she had brought them up to the roof of the house, and hid them
with the stalks of flax, which she had laid in order upon the roof.
 7 And the men pursued after them the way to Jordan unto the fords: and
as soon as they which pursued after them were gone out, they shut the
gate.
 8 ¶ And before they were laid down, she came up unto them upon the
roof;
 9 And she said unto the men, I know that the LORD hath given you the
land, and that your terror is fallen upon us, and that all the
inhabitants of the land faint because of you.
 10 For we have heard how the LORD dried up the water of the Red sea for
you, when ye came out of Egypt; and what ye did to the two kings of the
Amorites, that, _were_ on the other side Jordan, Sihon and Og, whom ye
utterly destroyed.
 11 And as soon as we had heard _these things,_ our hearts did melt,
neither did there remain any more courage in any man, because of you:
for the LORD your God, he _is_ God in heaven above, and in earth
beneath.
 12 Now therefore, I pray you, swear unto me by the LORD, since I have
shewed you kindness, that ye will also shew kindness unto my father's
house, and give me a true token:
 13 And _that_ ye will save alive my father, and my mother, and my
brethren, and my sisters, and all that they have, and deliver our lives
from death.
 14 And the men answered her, Our life for yours, if ye utter not this
our business. And it shall be, when the LORD hath given us the land,
that we will deal kindly and truly with thee.
 15 Then she let them down by cord through the window: for house _was_
upon the town wall, and she dwelt upon the wall.
 16 And she said unto them, Get you to the mountain, lest the pursuers
meet you; and hide yourselves there three days, until the pursuers be
returned: and afterward may ye go your way.
 17 And the men said unto her, We _will be_ blameless of this thine oath
which thou hast made us swear.
 18 Behold, _when_ we come into the land, thou shalt bind this line of
scarlet thread in the window which thou didst let us down by: and thou
didst let us down by: and thou shalt bring thy father, and thy mother,
and thy brethren, and all thy father's household, home unto thee.
 19 And it shall be, _that_ whosoever shall go out of the doors of thy
house into the street, his blood _shall be_ upon his head, and we _will
be_ guiltless: and whosoever shall be with thee in the house, his blood
_shall be_ on our head, if _any_ hand be upon him.
 20 And if thou utter this our business, then we will be quit of thine
oath which thou hast made us to swear.
 21 And she said, According unto your words, so _be_ it. And she sent
them away, and they departed: and she bound the scarlet line in the
window.
 22 And they went, and came unto the mountain, and abode there three
days, until the pursuers were returned: and the pursuers sought _them_
throughout all the way, but found _them_ not.
 23 ¶ So the two men returned, and descended from the mountain, and
passed over, and came to Joshua the son of Nun, and told him all
_things_ that befell them:
 24 And they said unto Joshua, Truly the LORD hath delivered into our
hands all the land; for even all the inhabitants of the country do faint
because of us.

CHAPTER 3

AND Joshua rose early in the morning; and they removed from Shittim, and
came to Jordan, he and all the children of Israel, and lodged there
before they passed over.
 2 And it came to pass after three days, that the officers went through
the host;
 3 And they commanded the people, saying, When ye see the ark of the
covenant of the LORD your God, and the priests the Levites bearing it,
then ye shall remove from your place, and go after it.
 4 Yet there shall be a space between you and it, about two thousand
cubits by measure: come not near unto it, that ye may know the way by
which ye must go: for ye have not passed _this_ way heretofore.
 5 And Joshua said unto the people, Sanctify yourselves: for to morrow
the LORD will do wonders among you.
 6 And Joshua spake unto the priests, sayingm Take up the ark of the
covenant, and pass over before the people. And they took up the ark of
the covenant, and went before the people.
 7 ¶ And the LORD said unto Joshua, This day will I begin to magnify
thee in the sight of all Israel, that they may know that, as I was with
Moses, _so_ I will be with thee.
 8 And thou shalt command the priest that bear the ark of the covenant,
saying, When ye are come to the brink of the water of Jordan, ye shall
stand still in Jordan.
 9 ¶ And Joshua said unto the children of Israel, Come hither, and hear
the words of the LORD your God.
 10 And Joshua said, Hereby ye shall know that the living God _is_ among
you, and _that_ he will without fail drive out from before you the
Canaanites, and the Hivites, and the Perizzites, and the Girgashites,
and the Amorites, and the Jebusites.
 11 Behold, the ark of the covenant of the Lord of all the earth passeth
over before you into Jordan.
 12 Now therefore take you twelve men out of the tribes of Israel, out
of every tribe a man.
 13 And it shall come to pass, as soon as the soles of the feet of the
priests that bear the ark of the LORD, the Lord of all the earth, shall
rest in the waters of Jordan, _that_ the waters of Jordan, _that_ the
waters of Jordan shall be cut off _from_ the waters that come down from
above; and they shall stand upon an heap.
 14 ¶ And it came to pass, when the people removed from their tents, to
pass over Jordan, and the priests bearing the ark of the covenant before
the people;
 15 And as they that bare the ark were come unto Jordan, and the priests
that bare the ark dipped in the brim of the water, (for Jordan
overfloweth all his banks all the time of harvest,)
 16 That the waters which came down from above stood _and_ rose up upon
an heap very far from the city Adam, that _is_ beside Zaretan: and those
that came down toward the sea of the plain, _even_ the salt sea, failed,
_and_ were cut off: and the people passed over right against Jericho.
 17 And the priests that bare the ark of the covenant of the LORD stood
firm on dry ground in the midst of Jordan, and all the people were
passed clean over Jordan.

CHAPTER 4

AND it came to pass, when all the people were clean passed over Jordan,
that the LORD spake unto Joshua, saying,
 2 Take you twelve men out of the people, out of every tribe a man,
 3 And command ye them, saying, Take you hence out of the midst of
Jordan, out of the place where the priests' feet stood firm, twelve
stones, and ye shall carry them over with you, and leave them in the
lodging place, where ye shall lodge this night.
 4 Then Joshua called the twelve men, whom he had prepared of the
children of Israel, out of every tribe a man:
 5 And Joshua said unto them, Pass over before the ark of the LORD your
God into the midst of Jordan, and take ye up every man of you a stone
upon his shoulder, according unto the number of the tribes of the
children of Israel:
 6 That this may be a sign among you, _that_ when your children ask
_their fathers_ in time to come, saying, What _mean_ ye by these stones?
 7  Then ye shall answer them, That the waters of Jordan were cut off
before the ark of the covenant of the LORD; when it passed over Jordan,
the waters of Jordan were cut off: and these stones shall be for a
memorial unto the children of Israel for ever.
 8 And the children of Israel did so as Joshua commanded, and took up
twelve stones out of the midst of Jordan, as the LORD spake unto Joshua,
according to the number of the tribes of the children of Israel, and
carried them over with them unto the place where they lodged, and laid
them down there.
 9 And Joshua set up twelve stones in the midst of Jordan, in the place
where the feet of the priests which bare the ark of the covenant stood:
and they are there unto this day.
 10 ¶ For the priests which bare the ark stood in the midst of Jordan,
until every thing was finished that the LORD commanded Joshua: and the
people hasted and passed over.
 11 And it came to pass, when all the people were clean passed over,
that the ark of the LORD passed over, and the priests, in the presence
of the people.
 12 And the children of Reuben, and the children of Gad, and the half
tribe of Manasseh, passed over armed before the children of Israel, as
Moses spake unto them:
 13 About forty thousand prepared for war passed over before the LORD
unto battle, to the plains of Jericho.
 14 ¶ On that day the LORD magnified Joshua in the sight of all Israel;
and they feared him, as they feared Moses, all the days of his life.
 15 And the LORD spake unto Joshua, saying,
 16 Command the priests that bear the ark of the testimony, that they
come up out of Jordan.
 17 Joshua therefore commanded the priests, saying, Come ye up out of
Jordan.
 18 And it came to pass, when the priests that bare the ark of the
covenant of the LORD were come up out of the midst of Jordan, _and_ the
soles of the priests' feet were lifted up unto the dry land, that the
waters of Jordan returned unto their place, and flowed over all his
banks, as _they did_ before.
 19 ¶ And the people came up out of Jordan on the tenth _day_ of the
first month, and encamped in Gilgal, in the east border of Jericho.
 20 And those twelve stones, which they took out of Jordan, did Joshua
pitch in Gilgal.
 21 And he spake unto the children of Israel, saying, When your children
shall ask their fathers in time to come, saying, What _mean_ these
stones?
 22 Then ye shall let your children know, saying, Israel came over this
Jordan on dry land.
 23 For the LORD your God dried up the waters of Jordan from before you,
until ye were passed over, as the LORD your God did to the Red sea,
which he dried up from before us, until we were gone over:
 24 That all the people of the earth might know the hand of the LORD,
that it _is_ might: that ye might fear the LORD your God for ever.

CHAPTER 5

AND it came to pass, when all the kings of the Amorites, which _were_ on
the side of Jordan westward, and all the kings of Canaanites, which
_were_ by the sea, heard that the LORD had dried up the waters of Jordan
from before the children of Israel, until we were passed over, that
their heart melted, neither was there spirit in them any more, because
of the children of Israel.
 2 ¶ At that time the LORD said unto Joshua, Make thee sharp knives, and
circumcise again the children of Israel the second time.
 3 And Joshua made him sharp knives, and circumcised the children of
Israel at the hill of the foreskins.
 4 And this _is_ the cause why Joshua did circumcise: All the people
that came out of Egypt, _that were_ males, _even_ all the men of war,
died in the wilderness by the way, after they came out of Egypt.
 5 Now all the people that came out were circumcised: but all the people
_that were_ born in the wilderness by the way as they came forth out of
Egypt, _them_ they had not circumcised.
 6 For the children of Israel walked forty years in the wilderness, till
all the people _that were_ men of war, which came out of Egypt, were
consumed, because they obeyed not the voice of the LORD: unto whom the
LORD sware unto their fathers that he would give us, a land that floweth
with milk and honey.
 7 And their children _whom_ he raised up in their stead, them Joshua
circumcised: for they were uncircumcised, because they had not
circumcised them by the way.
 8 And it came to pass, when they had done circumcising all the people,
that they abode in their places in the camp, till they were whole.
 9 And the LORD said unto Joshua, This day have I rolled away the
reproach of Egypt from off you. Wherefore the name of the place is
called Gilgal unto this day.
 10 ¶ And the children of Israel encamped in Gilgal, and kept the
passover on the fourteenth day of the month at even in the plains of
Jericho.
 11 And they did eat of the old corn of the land on the morrow after the
passover, unleavened cakes, and parched _corn_ in the selfsame day.
 12 ¶ And the manna ceased on the morrow after they had eaten of the old
corn of the land; neither had the children of Israel manna any more; but
they did eat of the fruit of the land of Canaan that year.
 13 ¶ And it came to pass, when Joshua went unto him, and said unto him,
_Art_ thou for us, or for our adversaries?
 14 And he said, Nay; but as captain of the host of the LORD am I now
come. And Joshua fell on his face to the earth, and di worship, and said
unto him, What saith my lord unto his servant?
 15 And the captain of the LORD'S host said unto Joshua, Loose thy shoe
from off thy foot; for the place whereon thou standest _is_ holy. And
Joshua did so.

CHAPTER 6

NOW Jericho was straightly shut up because of the children of Israel:
none went out, and none came in.
 2 And the LORD said unto Joshua, See, I have given into thine hand
Jericho, and the king thereof, _and_ the mighty men of valour.
 3 And ye shall compass the city, all _ye_ men of war, _and_ go round
about the city once. Thus shalt thou do six days.
 4 And seven priests shall bear before the ark seven trumpets of rams'
horns: and the seventh day ye shall compass the city seven times, and
the priests shall blow with the trumpets.
 5 And it shall come to pass, that when they make a long blast with the
ram's horn, _and_ when ye hear the sound of the trumpet, all the people
shall shout with a great shout; and the wall of the city shall fall down
flat, and the people shall ascend up every man straight before him.
 6 ¶ And Joshua the son of Nun called the priests, and said unto them,
Take up the ark of the covenant, and let seven priests bear seven
trumpets of rams' horns before the ark of the LORD.
 7 And he said unto the people, Pass on, and compass the city, and let
him that is armed pass on before the ark of the LORD.
 8 ¶ And it came to pass, when Joshua had spoken unto the people, that
the seven priests bearing the seven trumpets of rams' horns passed on
before the LORD, and blew with the trumpets: and the ark of the covenant
of the LORD followed them.
 9 ¶ And the armed men went before the priests that blew with the
trumpets, and the rereward came after the ark, _the priests_ going on,
and blowing with the trumpets.
 10 And Joshua had commanded the people, saying, Ye shall not shout, nor
make any noise with your voice, neither shall _any_ word proceed out of
your mouth, unti the day I bid you shout; then shall ye shout.
 11 So the ark of the LORD compassed the city, going about _it_ once:
and they came into the camp, and lodged in the camp.
 12 ¶ And Joshua rose early in the morning, and the priests took up the
ark of the LORD.
 13 And seven priests bearing seven trumpets of rams' horns before the
ark of the LORD went on continually, and blew with the trumpets: and the
armed men went before them; but the rereward came after the ark of the
LORD, _the priests_ going on, and blowing with the trumpets.
 14 And the second day they compassed the city once, and returned into
the camp: so they did six days.
 15 And it came to pass on the seventh day, that they rose early about
the dawning of the day, and compassed the city after the same manner
seven times: only on that day they compassed the city seven times.
 16 And it came to pass at the seventh time, when the priests blew with
the trumpets, Joshua said unto the people, Shout; for the LORD hath
given you the city.
 17 ¶ And the city shall be accursed, _even_ it, and all that _are_
therein, to the LORD: only Rahab the harlot shall live, she and all that
_are_ with her in the house, because she hid the messengers that we
sent.
 18 And ye, in any wise keep _yourselves_ from the accursed thing, lest
ye take of the accursed thing, and make the camp of Israel a curse, and
trouble it.
 19 But all the silver, and gold, and vessels of brass and iron, _are_
consecrated unto the LORD: they shall come into the treasury of the
LORD.
 20 So the people shouted when _the priests_ blew with the trumpets: and
it came to pass, when the people heard the sound of the trumpet, and the
people shouted with a great shout, that the wall fell down flat, so that
the people went up into the city, every man straight before him, and
they took the city.
 21 And they utterly destroyed all that _was_ in the city, both man and
woman, young and old, and ox, and sheep, and ass, with the edge of the
sword.
 22 But Joshua had said unto the two men that had spied out the country,
Go into the harlot's house, and bring out thence the woman, and all that
she hath, as ye sware unto her.
 23 And the young men that were spies went in, and brought out Rahab,
and her father, and her mother, and her brethren, and all that she had;
and theybrought out all her kindred, and left them without the camp of
Israel.
 24 And they burnt the city with fire, and all that _was_ therein: only
the silver, and the gold, and the vessels of brass and of iron, they put
into the treasury of the house of the LORD.
 25 And Joshua saved Rahab the harlot alive, and her father's household,
and all that she had; and she dwelleth in Israel _even_ unto this day;
because she hid the messengers, which Joshua sent to spy out Jericho.
 26 ¶ And Joshua adjured _them_ at that time, saying, Cursed _be_ the
man before the LORD, that riseth up and buildeth this city Jericho: he
shall lay the foundation thereof in his firstborn, and in his youngest
_son_ shall he set up the gates of it.
 27 So the LORD was with Joshua; and his fame was _noised_ throughout
all the country.

CHAPTER 7

BUT the children of Israel committed a trespass in the accursed thing:
for Achan, the son of Carmi, the son of Zabdi, the son of Zerah, of the
tribe of Judah, tool of the accursed thing: and the anger of the LORD
was kindled against the children of Israel.
 2 And Joshua sent men from Jericho to Ai, which _is_ beside Bethaven,
on the east side of Bethel, and spake unto them, saying, Go up and view
the country. And the men went up and viewed Ai.
 3 And they returned to Joshua, and said unto him, Let not all the
people go up; but let about two or three thousand men go up and smite
Ai; _and_ make not all the people to labour thither; for they _are but_
few.
 4 So there went up thither of the people about three thousand men: and
they fled before the men of Ai.
 5 And the men of Ai smote of them about thirty and six men: for they
chased them _from_ before the gate _even_ unto Shebarim, and smote them
in the going down: wherefore the hearts of the people melted, and became
as water.
 6 ¶ And Joshua rent his clothes, and fell to the earth upon his face
before the ark of the LORD until the eventide, he and the elders of
Israel, and put dust upon their heads.
 7 And Joshua said, Alas, O Lord GOD, wherefore hast thou at all brought
this people over Jordan, to deliver us into the hand of the Amorites, to
destroy us? would to God we had been content, and dwelt on the other
side Jordan!
 8 O Lord, what shall I say, when Israel turneth their backs before
their enemies!
 9 For the Canaanites and all the inhabitants of the land shall hear _of
it,_ and shall environ us round, and cut off our name from the earth:
and what wilt thou do unto thy great name?
 10 ¶ And the LORD said unto Joshua, Get thee up; wherefore liest thou
thus upon thy face?
 11 Israel hath sinned, and they have transgressed my covenant which I
commanded them: for they have even taken of the accursed thing, and have
also stolen, and dissembled also, and they have put _it_ even among
their own stuff.
 12 Therefore the children of Israel could not stand before their
enemies, _but_ turned _their_ backs before their enemies, because they
were accursed: neither will I be with youany more, except ye destroy the
accursed from among you.
 13 Upm sanctify the people, and say, Sanctify yourselves against to
morrow: for thus saith the LORD God of Israel, _There is_ an accursed
thing in the midst of thee, O Israel: thou canst not stand before thine
enemies, until ye take away the accursed thing from among you.
 14 In the morning therefore ye shall be brought according to your
tribes: and it shall come according to the families _thereof;_ and the
family which the LORD shall take shall come byu households; and the
household which the LORD shall take shall come man by man.
 15 And it shall be, _that_ he that is taken with the accursed thing
shall be burnt with fire, he and all that he hath: because he hath
transgressed the covenant of the LORD, and because he hath wrought folly
in Israel.
 16 ¶ So Joshua rose up early in the morning, and brought Israel by
their tribes; and the tribe of Judah was taken:
 17 And he brought the family of Judah; and he took the family of the
Zarhites: and he brought the family of the Zarhites man by man; and
Zabdi was taken:
 18 And he brought his household man by man; and Achan, the son of
Carmi, the son of Zabdi, the son of Zerah, of the tribe of Judah, was
taken.
 19 And Joshua said unto Achan, My son, give, I pray thee, glory to the
LORD God of Israel, and make confession unto him; and tell me now what
thou hast done; hide _it_ not from me.
 20 And Achan answered Joshua, and said, Indeed I have sinned against
the LORD God of Israel, and thus and thus have I done:
 21 When I saw among the spoils a goodly Babylonish garment, and two
hundred shekels of silver, and a wedge of gold of fifty shekels weight,
then I coveted them, and took them; and, behold, they _are_ hid in the
earth in the midst of my tent, and the silver under it.
 22 ¶ So Joshua sent messengers, and they ran unto the tent; and,
behold, _it was_ hid in his tent, and the silver under it.
 23 And they took them out of the midst of the tent, and brought them
unto Joshua, and unto all the children of Israel, and laid them out
before the LORD.
 24 And Joshua, and all Israel with him, took Achan the son of Zerah,
and the silver, and the garment, and the wedge of gold, and his sons and
his daughters, and his oxen, and his asses, and his sheep, and his tent,
and all that he had: and they brought them unto the valley of Achor.
 25 And Joshua said, Why hast thou troubled us? the LORD shall trouble
thee this day. And all Israel stoned him with stones, and burned them
with fire, after they had stoned them with stones.
 26 And they raised over him a great heap of stones unto this day. So
the LORD turned from the fierceness of his anger. Wherefore the name of
that place was called, The valley of Achor, unto this day.

CHAPTER 8

AND the LORD said unto Joshua, Fear not, neither be thou dismayed: take
all the people of war with thee, and arise, go up to Ai: see, I have
given into thy hand the king of Ai, and his people, and his city, and
his land:
 2 And thou shalt do to Ai and her king as thou didst unto Jericho and
her king: only the spoil thereof, and the cattle thereof, shall ye take
for a prey unto yourselves: lay thee an ambush for the city behind it.
 3 ¶ So Joshua arose, and all the people of war, to go up against Ai:
and Joshua chose out thirty thousand mighty men of valour, and sent them
away by night.
 4 And he commanded them, saying, Behold, ye shall lie in wait against
the city, _even_ behind the city: go not very far from the city, but be
ye all ready:
 5 And I, and all the people that _are_ with me, will approach unto the
city: and it shall come to pass, when they come out against us, as at
the first, that we will flee before them,
 6 (For they will come out after us) till we have drawn them from the
city; for they will say, They flee before us, as at the first: therefore
we will flee before them.
 7 Then ye shall rise up from the ambush, and seize upon the city: for
the LORD your God will deliver it into your hand.
 8 And it shall be, when ye have taken the city, _that_ ye shall set the
city on fire: according to the commandment of the LORD shall ye do. See,
I have commanded you.
 9 ¶ Joshua therefore sent them forth: and they went to lie in ambush,
and abode between Bethel and Ai, on the west side of Ai: but Joshua
lodged that night among the people.
 10 And Joshua rose up early in the morning, and numbered the people,
and went up, he and the elders of Israel, before the people to Ai.
 11 And all the people, _even the people_ of war that _were_ with him,
went up, and drew nigh, and came before the city, and pitched on the
north side of Ai: now _there was_ a valley between them and Ai.
 12 And he took about five thousand men, and set them to lie in ambush
between Beth-el and Ai, on the west side of the city.
 13 And when they had set the people, _even_ all the host that _was_ on
the north of the city, and their liers in wait on the west of the city,
Joshua went that night into the midst of the valley.
 14 ¶ And it came to pass, when the king of Ai saw _it,_ that they
hasted and rose up early, and the men of the city went out against
Israel to battle, he and all his people, at a time appointed, before the
plain but he wist not that _there were_ liers in ambush against him
behind the city.
 15 And Joshua and all Israel made as if they were beaten before them,
and fled by the way of the wilderness.
 16 And all the people that _were_ in Ai were called together to pursue
after them: and they pursued after Joshua, and were drawn away from the
city.
 17 And there was not a man left in Ai or in Beth-el, that went not out
after Israel: and they left the city open, and pursued after Israel.
 18 And the LORD said unto Joshua, Stretch out the spear that _is_ in
thy hand toward Ai; for I will give it into thine hand. And Joshua
stretched out the spear that _he had_ in his hand toward the city.
 19 And the ambush arose quickly out of their place, and they ran as
soon as he had stretched out his hand: and they entered into the city,
and took it, and hasted and set the city on fire.
 20 And when the men of Ai looked behind them, they saw, and, behold,
the smoke of the city ascended up to heaven, and they had no power to
flee this way or that way: and the people that fled to the wilderness
turned back upon the pursuers.
 21 And when Joshua and all Israel saw that the ambush had taken the
city. and that the smoke of the city ascended, then they turned again,
and slew the men of Ai.
 22 And the other issued out of the city against them; so they were in
the midst of Israel, some on this side, and some on that side: and they
smote them, so that they let none of them remain or escape.
 23 And the king of Ai they took alive, and brought him to Joshua.
 24 And it came to pass, when Israel had made an end of slaying all the
inhabitants of Ai in the field, in the wilderness wherein they chased
them, and when they were all fallen on the edge of the sword, until they
were consumed, that all the Israelites returned unto Ai, and smote it
with the edge of the sword.
 25 And so it was, _that_ all that fell that day, both of men and women,
_were_ twelve thousand, _even_ all the men of Ai.
 26 For Joshua drew not his hand back, wherewith he stretched out the
spear, until he had utterly destroyed all the inhabitants of Ai.
 27 Only the cattle and the spoil of that city Israel took for a prey
unto themselves, according unto the word of the LORD which he commanded
Joshua.
 28 And Joshua burnt Ai, and made it an heap for ever, _even_ a
deslation unto this day.
 29 And the king of Ai he hanged on a tree until eventide: and as soon
as the sun was down, Joshua commanded that they should take his carcase
down from the tree, and cast it at the entering of the gate of the city,
and raise thereon a great heap of stones, _that remaineth_ unto this
day.
 30 ¶ Then Joshua built an altar unto the LORD God of Israel in mount
Ebal,
 31 As Mooses the servant of the LORD commanded the children of Israel,
as it is written in the book of the law of Moses, an altar of whole
stones, over which no man hath lift up _any_ iron: and they offered
thereon burnt offerings unto the LORD, and sacrificed peace offerings.
 32 ¶ And he wrote there upon the stones a copy of the law of Moses,
which he wrote in the presence of the children of Israel.
 33 And all Israel, and their elders, and officers, and their judges,
stood on this side the ark and on that side before the priests the
Levites, which bare the ark of the covenant of the LORD, as well the
stranger, as he that was born among them; half of them over against
mount Gerizim, and half of them over against mount Ebal; as Moses the
servant of the LORD had commanded before, that they should bless the
people of Israel.
 34 And afterward he read all the words of the law, the blessings and
the cursings, according to all that is written in the book of the law.
 35 There was not a word of all that Moses commanded, which Joshua read
not before all the congregation of Israel, with the women, and the
little ones, and the strangers that were conversant among them.

CHAPTER 9

AND it came to pass, when all the kings which _were_ on this side
Jordan, in the hills, and in the valleys, and in all the coasts of the
great sea over against Lebanon, the Hittite, and the Amorite, the
Canaanite, the Perizzite, the Hivite, and the Jebusite, heard _thereof;_
 2 That they gathered themselves together, to fight with Joshua and with
Israel, with one accord.
 3 ¶ And when the inhabitants of Gibeon heard what Joshua had done unto Jericho and to Ai.
 4 They did work wilily, and went and made as if they had been ambassadors, and took old sacks upon their asses, and wine bottles, old, and rent, and bound up;
 5 And old shoes and clouted upon their feet, and old garments upon them; and all the bread of their provision was dry _and_ mouldy.
 6 And they went to Joshua unto the camp at Gilgal, and said unto him, and to the men of Israel, We be come from a far country: now therefore make ye a league with us.
 7 And the men of Israel said unto the Hivites, Peradventure ye dwell among us; and how shall we make a league with you?
 8 And they said unto Joshua, We _are_ thy servants. And Joshua said unto them, Who _are_ ye? and from whence come ye?
 9 And they said unto him, From a very far country thy servants are come because of the name of the LORD thy God: for we have heard the fame of him, and all that he did in Egypt,
 10 And all that he did to the two kings of the Amorites, that _were_ beyond Jordan, to Sihon king of Heshbon, and to Og king of Bashan, which _was_ at Ashtaroth.
 11 Wherefore our elders and all the inhabitants of our country spake to us, saying, Take victuals with you for the journey, and go to meet them, and say unto them, We _are_ your servants: therefore now make ye a league with us.
 12 This is our bread we took hot _for_ our provision out of our houses on the day we came forth to go unto you; but now, behold, it is dry, and it is mouldy:
 13 And these bottles of wine, which we filled, _were_ new; and, behold, they be rent: and these our garments and our shoes are become old by reason of the very long journey.
 14 And the men took of their victuals, and asked not _counsel_ at the mouth of the LORD.
 15 And Joshua made peace with them, and made a league with them, to let them live: and the princes of the congregation sware unto them.
 16 ¶ And it came to pass at the end of three days after they had made a league with them, that they heard that they _were_ their neigbours, and _that_ they dwelt among them.
 17 And the children of Israel journeyed, and came unto their cities on the third day. Now their cities _were_ Gibeon, and Chephirah, and Beeroth, and Kirjathjearim.
 18 And the children of Israel smote them not, because the princes of the congregation had sworn unto them by the LORD God of Israel. And all the congregation murmured against the princes.
 19 But all the princes said unto all the congregation, We have sworn unto them by the LORD God of Israel: now therefore we may not touch them.
 20 This we will do to them; we will even let them live, lest wrath be upon us, because of the oath which we sware unto them.
 21 And the princes said unto them, Let them live; but let them be hewers of wood and drawers of water unto all the congregation; as the princes had promised them.
 22 ¶ And Joshua called for them, and he spake unto them, saying, Wherefore have ye beguiled us, saying, We _are_ very far from you; when ye dwell among us?
 23 Now therefore ye _are_ cursed, and there shall none of you be freed from being bondmen, and hewers of wood and drawers of water for the house of my God.
 24 And they answered Joshua, and said, Because it was certainly told thy servants, how that the LORD thy God commanded his servant Moses to give you all the land from before you, therefore we were sore afraid of our lives because of you, and have done this thing.
 25 And now, behold, we _are_ in thine hand: as it seemeth good and right unto thee to do unto us, do.
 26 And so did he unto them, and delivered them out of the hand of the children of Israel, that they slew them not.
 27 And Joshua made them that day hewers of wood and drawers of water for the congregation, and for the altar of the LORD, even unto this day, in the place which he should choose.

CHAPTER 10

NOW it came to ass, when Adonizedek king of Jerusalem had heard how Joshua had taken Ai, and had utterly destroyed it; as he had done to Jericho and her king, so he had done to Ai and her king; and how the inhabitants of Gibeon had made peace with Israel, and were among them.
