import re
def words(t): return [w.lower() for w in re.findall(r"\b[\w']+\b",t)]
def lwords(t): return [w.lower() for w in re.findall(r"\b[a-z][\w']*\b",t)]

def vocab(t): return set(words(t))

# >>>> from bible import *
# >>> from parle import *
# >>> from words import *
# >>> dir(re)
# ['A', 'ASCII', 'DEBUG', 'DOTALL', 'I', 'IGNORECASE', 'L', 'LOCALE', 'M', 'MULTILINE', 'Match', 'Pattern', 'RegexFlag', 'S', 'Scanner', 'T', 'TEMPLATE', 'U', 'UNICODE', 'VERBOSE', 'X', '_MAXCACHE', '__all__', '__builtins__', '__cached__', '__doc__', '__file__', '__loader__', '__name__', '__package__', '__spec__', '__version__', '_cache', '_compile', '_compile_repl', '_expand', '_locale', '_pickle', '_special_chars_map', '_subx', 'compile', 'copyreg', 'enum', 'error', 'escape', 'findall', 'finditer', 'fullmatch', 'functools', 'match', 'purge', 'search', 'split', 'sre_compile', 'sre_parse', 'sub', 'subn', 'template']
# >>> 
# >>> t=File('net.txt').text
# >>> 
# >>> netws=set(lwords(File('net.txt').text))
# >>> e10000=vocab(File('google-10000-english.txt').text)
# >>> w467k=set(map(F(method.lower)@F(method.strip),File('words467k.txt').lines))
# >>> len(netws),len(w467k)
# (11588, 466547)
# >>> l='\n'.join(sorted(netws-w467k))
# >>> File('netun').text=l
# >>> 
# >>> 
# >>> 
