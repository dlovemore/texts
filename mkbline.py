>>> from bible import *
>>> Str=F(str)
>>> Sum=F(sum)
>>> titles=[0]+File('titles').lines
>>> supers=[0]+File('superscriptions').lines
>>> letters=[0]+File('letters').lines
>>> colophons=File('colophons').lines
>>> extras=File('extras').lines
>>> 
>>> 
>>> def psalmt(n): return ['PSALM',str(n)]+[supers[n]]*bool(supers[n])
... 
>>> def chapt(v): return psalmt(v.ch()) if v.bookn()==19 else ['CHAPTER',str(v.ch())]
... 
>>> 
>>> def headt(v): return (([titles[v.bookn()]] if v.cav()==(1,1) else [])+(chapt(v) if v.verse()==1 and b.book(v.bookn()).chc()>1 else [])+([letters[v.verse()//8+1]] if v.ch()==119 and v.verse()%8==1 else []))
... 
>>> def vnt(v): return ' '.join(headt(v) + [str(v.verse())])
... 
>>> def vnt1(v):
...     r=[]
...     c,vn = v.cav()
...     bn=v.bookn()
...     b=v.book(bn)
...     if vn==1==c:
...         if bn==1: r+=extras[1].split()
...         if bn==40: r+=extras[2].split()
...         if 45<bn<=45+14:
...             r.append(colophons[bn-46])
...         r.append(titles[bn])
...     if b.chc()>1:
...         if vn==1:
...             r.append('PSALM' if bn==19 else 'CHAPTER')
...             r.append(str(c))
...             if bn==19:
...                 super=supers[c]
...                 if super: r.append(super)
...         if bn==19 and c==119:
...             if vn%8==1: r.append(letters[vn//8+1])
...     if vn>1:
...         r.append(str(vn))
...     r.append(v.text())
...     if bn==66 and c==22 and vn==21: r+=extras[3].split()
...     return r
... 
>>> 
>>> File('wline').lines=b.words()
>>> 
>>> vnt=F(vnt1)
>>> vt=vnt*F(' '.join)
>>> 
>>> wordperline=I@vt*F(' '.join)*words
>>> 
>>> File('bline').lines=b*wordperline@meth.replace('-','')@meth.replace('(','')
>>> 
>>> 
>>> 
