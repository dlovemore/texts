>>> from bible import *
>>> cvs=[(int(mo[1]),int(mo[2])) for mo in re.finditer(r'[^-](\d+):(\d+)\s',File('net.txt').text)]
>>> cvs[:10]
[(1, 1), (1, 2), (1, 3), (1, 4), (1, 5), (1, 6), (1, 7), (1, 8), (1, 9), (1, 10)]
>>> len(cvs)
31087
>>> def chcvs():
...     ch=1
...     b=Sel(bible,[])
...     for (i,j),(p,q) in pairs(cvs):
...         if (p==1 and q==1): ch+=1; continue
...         if (p==i and q==j+1) or (p==i+1 and q==1): continue
...         # print([ch,i,j+1])
...         b|=bible[ch][i:j+1]
...     return b
... 
>>> 
>>> 
>>> chcvs()
Matthew 17:21;18:11;23:14;Mark 7:16;9:44,46;11:26;15:28;Luke 17:36;23:17;John 5:4;Acts 8:37;15:34;24:7;28:29;Romans 16:24 (16 verses)
>>> p(_)
Matthew 17:21 Howbeit this kind goeth not out but by prayer and fasting.
Matthew 18:11 For the Son of man is come to save that which was lost.
Matthew 23:14 Woe unto you, scribes and Pharisees, hypocrites! for ye devour widows' houses, and for a pretence make long prayer: therefore ye shall receive the greater damnation.
Mark 7:16 If any man have ears to hear, let him hear.
Mark 9:44 Where their worm dieth not, and the fire is not quenched.
Mark 9:46 Where their worm dieth not, and the fire is not quenched.
Mark 11:26 But if ye do not forgive, neither will your Father which is in heaven forgive your trespasses.
Mark 15:28 And the scripture was fulfilled, which saith, And he was numbered with the transgressors.
Luke 17:36 Two men shall be in the field; the one shall be taken, and the other left.
Luke 23:17 (For of necessity he must release one unto them at the feast.)
John 5:4 For an angel went down at a certain season into the pool, and troubled the water: whosoever then first after the troubling of the water stepped in was made whole of whatsoever disease he had.
Acts 8:37 And Philip said, If thou believest with all thine heart, thou mayest. And he answered and said, I believe that Jesus Christ is the Son of God.
Acts 15:34 Notwithstanding it pleased Silas to abide there still.
Acts 24:7 But the chief captain Lysias came upon us, and with great violence took him away out of our hands,
Acts 28:29 And when he had said these words, the Jews departed, and had great reasoning among themselves.
Romans 16:24 The grace of our Lord Jesus Christ be with you all. Amen.
>>> 
>>> 
